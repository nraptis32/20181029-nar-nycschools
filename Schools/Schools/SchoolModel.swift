//
//  SchoolModel.swift
//  Schools
//
//  Created by Nicholas Raptis on 10/30/18.
//  Copyright © 2018 Chase. All rights reserved.
//

import UIKit

class SchoolModel {
    
    //Used by CSVStripper to store program
    //readable version of school data.
    
    //Colz | CSV File Rowz
    var uuid = "" // 0
    var name = "" // 1
    var description = "" // 3
    var sports1 = "" // 33
    var sports2 = "" // 34
    var sports3 = "" // 35
    var zone = "" // 460
    var city = "" // 461
    
    var didMatch: Int = 0
    
    var satTakerCount = 0
    var satReadingAvg = 0
    var satMathAvg = 0
    var satWritingAvg = 0
}
