//
//  SchoolDetailViewController.swift
//  Schools
//
//  Created by Nicholas Raptis on 10/31/18.
//  Copyright © 2018 Chase. All rights reserved.
//

import UIKit
import CoreData

class SchoolDetailViewController: UIViewController {
    
    let titleFont = UIFont(name: "HelveticaNeue-Bold", size: 19.0)!
    let headerFont = UIFont(name: "HelveticaNeue-Bold", size: 14.0)!
    let detailFont = UIFont(name: "HelveticaNeue-Thin", size: 14.0)!
    let descriptionFont = UIFont(name: "HelveticaNeue", size: 14.0)!
    
    var scrollView: UIScrollView!
    
    var nameLabel: UILabel!
    
    var descriptionLabel: UILabel!
    
    
    
    var satBubble: InfoBubbleView!
    
    var satLabelTestScores: UILabel!
    
    var satLabelTitleReading: UILabel!
    var satLabelTitleMath: UILabel!
    var satLabelTitleWriting: UILabel!
    
    var satLabelScoreReading: UILabel!
    var satLabelScoreMath: UILabel!
    var satLabelScoreWriting: UILabel!
    
    
    var sportsTitleLabel: UILabel!
    var sportsLabel1: UILabel!
    var sportsLabel2: UILabel!
    var sportsLabel3: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    /// We essentially place a lot of rectangles into
    /// their correct positions to layout this dynamically
    /// sizing content. Height will vary depending on managed
    /// content which we are displaying.
    ///
    /// - Parameter managed: The managed school being displayed.
    func setUpWith(managed: NSManagedObject) {
        
        var name = managed.value(forKeyPath: "name") as! String
        name = name.trimmingCharacters(in: CharacterSet(charactersIn: "\""))
        
        
        var descr = managed.value(forKeyPath: "long_description") as! String
        descr = descr.trimmingCharacters(in: CharacterSet(charactersIn: "\""))
        
        var scoreReading = "N/A"
        if let score = managed.value(forKeyPath: "sat_reading") as? Int {
            if score > 0 { scoreReading = "\(score)" }
        }
        
        var scoreMath = "N/A"
        if let score = managed.value(forKeyPath: "sat_math") as? Int {
            if score > 0 { scoreMath = "\(score)" }
        }
        
        var scoreWriting = "N/A"
        if let score = managed.value(forKeyPath: "sat_writing") as? Int {
            if score > 0 { scoreWriting = "\(score)" }
        }
        
        var sports1 = ""
        if var sport = managed.value(forKeyPath: "sports1") as? String {
            sport = sport.trimmingCharacters(in: .whitespacesAndNewlines)
            sport = sport.trimmingCharacters(in: CharacterSet(charactersIn: "\""))
            if sport.count > 0 {
                sports1 = sport
            }
        }
        
        var sports2 = ""
        if var sport = managed.value(forKeyPath: "sports2") as? String {
            sport = sport.trimmingCharacters(in: .whitespacesAndNewlines)
            sport = sport.trimmingCharacters(in: CharacterSet(charactersIn: "\""))
            if sport.count > 0 {
                sports2 = sport
            }
        }
        
        var sports3 = ""
        if var sport = managed.value(forKeyPath: "sports3") as? String {
            sport = sport.trimmingCharacters(in: .whitespacesAndNewlines)
            sport = sport.trimmingCharacters(in: CharacterSet(charactersIn: "\""))
            if sport.count > 0 {
                sports3 = sport
            }
        }
        
        // If layout constraints go bad, default to this "dummy rect"
        // so we can see the problem.
        let dummyRect = CGRect(x: 0.0, y: 0.0, width: 255.0, height: 255.0)
        
        var top: CGFloat = 34.0
        
        scrollView = UIScrollView(frame: dummyRect)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.isOpaque = false
        scrollView.backgroundColor = UIColor.clear
        view.addSubview(scrollView)
        
        let scrollViewLeftConstraint = NSLayoutConstraint(item: scrollView, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1.0, constant: 0.0)
        let scrollViewRightConstraint = NSLayoutConstraint(item: scrollView, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1.0, constant: 0.0)
        let scrollViewTopConstraint = NSLayoutConstraint(item: scrollView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 0.0)
        let scrollViewBottomConstraint = NSLayoutConstraint(item: scrollView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        
        view.addConstraints([scrollViewLeftConstraint, scrollViewRightConstraint, scrollViewTopConstraint, scrollViewBottomConstraint])
        
        nameLabel = UILabel(frame: dummyRect)
        nameLabel.textAlignment = .center
        nameLabel.numberOfLines = 0
        nameLabel.font = titleFont
        nameLabel.text = name
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(nameLabel)
        let titleWidth: CGFloat = view.bounds.width - 46.0
        let titleHeight: CGFloat = CGFloat(UILabel.getHeight(ofString: name, font: titleFont, width: Float(titleWidth), maxLines: 0)) + 3.0
        let nameLabelTopConstraint = NSLayoutConstraint(item: nameLabel, attribute: .top, relatedBy: .equal, toItem: scrollView, attribute: .top, multiplier: 1.0, constant: top)
        let nameLabelCenterXConstraint = NSLayoutConstraint(item: nameLabel, attribute: .centerX, relatedBy: .equal, toItem: scrollView, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        let nameLabelWidthConstraint = NSLayoutConstraint(item: nameLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: titleWidth)
        let nameLabelHeightConstraint = NSLayoutConstraint(item: nameLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: titleHeight)
        nameLabel.addConstraints([nameLabelWidthConstraint, nameLabelHeightConstraint])
        scrollView.addConstraints([nameLabelTopConstraint, nameLabelCenterXConstraint])

        top += titleHeight + 6.0
        
        descriptionLabel = UILabel(frame: dummyRect)
        descriptionLabel.font = descriptionFont
        descriptionLabel.numberOfLines = 0
        descriptionLabel.text = descr
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(descriptionLabel)
        let descriptionWidth: CGFloat = view.bounds.width - 30.0
        let descriptionHeight: CGFloat = CGFloat(UILabel.getHeight(ofString: descr, font: descriptionFont, width: Float(descriptionWidth), maxLines: 0)) + 3.0
        let descriptionLabelTopConstraint = NSLayoutConstraint(item: descriptionLabel, attribute: .top, relatedBy: .equal, toItem: scrollView, attribute: .top, multiplier: 1.0, constant: top)
        let descriptionLabelCenterXConstraint = NSLayoutConstraint(item: descriptionLabel, attribute: .centerX, relatedBy: .equal, toItem: scrollView, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        let descriptionLabelWidthConstraint = NSLayoutConstraint(item: descriptionLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: descriptionWidth)
        let descriptionLabelHeightConstraint = NSLayoutConstraint(item: descriptionLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: descriptionHeight)
        descriptionLabel.addConstraints([descriptionLabelWidthConstraint, descriptionLabelHeightConstraint])
        scrollView.addConstraints([descriptionLabelTopConstraint, descriptionLabelCenterXConstraint])
        
        top += descriptionHeight + 10.0
        
        satLabelTestScores = UILabel(frame: dummyRect)
        satLabelTestScores.font = headerFont
        satLabelTestScores.textAlignment = .center
        satLabelTestScores.text = "SAT Scores"
        satLabelTestScores.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(satLabelTestScores)
        let satLabelTopConstraint = NSLayoutConstraint(item: satLabelTestScores, attribute: .top, relatedBy: .equal, toItem: scrollView, attribute: .top, multiplier: 1.0, constant: top)
        let satLabelCenterXConstraint = NSLayoutConstraint(item: satLabelTestScores, attribute: .centerX, relatedBy: .equal, toItem: scrollView, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        let satLabelWidthConstraint = NSLayoutConstraint(item: satLabelTestScores, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 220.0)
        let satLabelHeightConstraint = NSLayoutConstraint(item: satLabelTestScores, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 19.9)
        satLabelTestScores.addConstraints([satLabelWidthConstraint, satLabelHeightConstraint])
        scrollView.addConstraints([satLabelTopConstraint, satLabelCenterXConstraint])
        
        top += 28.0
        
        satBubble = InfoBubbleView(frame: dummyRect)
        satBubble.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(satBubble)
        
        let satBubbleWidth = view.bounds.size.width - 36.0
        let satLabelHeight: CGFloat = 28.0
        let satLabelWidth: CGFloat = satBubbleWidth / 3.0
        let satBubbleHeight: CGFloat = 8.0 + (satLabelHeight * 2.0)
        
        let satBubbleTopConstraint = NSLayoutConstraint(item: satBubble, attribute: .top, relatedBy: .equal, toItem: scrollView, attribute: .top, multiplier: 1.0, constant: top)
        let satBubbleCenterXConstraint = NSLayoutConstraint(item: satBubble, attribute: .centerX, relatedBy: .equal, toItem: scrollView, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        
        let satBubbleWidthConstraint = NSLayoutConstraint(item: satBubble, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: satBubbleWidth)
        let satBubbleHeightConstraint = NSLayoutConstraint(item: satBubble, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: satBubbleHeight)
        
        satBubble.addConstraints([satBubbleWidthConstraint, satBubbleHeightConstraint])
        scrollView.addConstraints([satBubbleTopConstraint, satBubbleCenterXConstraint])
        
        
        
        satLabelTitleReading = UILabel(frame: dummyRect)
        satLabelTitleReading.textAlignment = .center
        satLabelTitleReading.textColor = UIColor.white
        satLabelTitleReading.font = headerFont
        satLabelTitleReading.text = "Reading"
        satLabelTitleReading.translatesAutoresizingMaskIntoConstraints = false
        
        satBubble.addSubview(satLabelTitleReading)
        let readingTopConstraint = NSLayoutConstraint(item: satLabelTitleReading, attribute: .top, relatedBy: .equal, toItem: satBubble, attribute: .top, multiplier: 1.0, constant: 7.0)
        let readingLeftConstraint = NSLayoutConstraint(item: satLabelTitleReading, attribute: .left, relatedBy: .equal, toItem: satBubble, attribute: .left, multiplier: 1.0, constant: 0.0)
        let readingWidthConstraint = NSLayoutConstraint(item: satLabelTitleReading, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: satLabelWidth)
        let readingHeightConstraint = NSLayoutConstraint(item: satLabelTitleReading, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: satLabelHeight)
        satLabelTitleReading.addConstraints([readingWidthConstraint, readingHeightConstraint])
        satBubble.addConstraints([readingTopConstraint, readingLeftConstraint])
        
        satLabelTitleMath = UILabel(frame: dummyRect)
        satLabelTitleMath.textAlignment = .center
        satLabelTitleMath.textColor = UIColor.white
        satLabelTitleMath.font = headerFont
        satLabelTitleMath.text = "Math"
        satLabelTitleMath.translatesAutoresizingMaskIntoConstraints = false
        
        satBubble.addSubview(satLabelTitleMath)
        let mathTopConstraint = NSLayoutConstraint(item: satLabelTitleMath, attribute: .top, relatedBy: .equal, toItem: satBubble, attribute: .top, multiplier: 1.0, constant: 7.0)
        let mathLeftConstraint = NSLayoutConstraint(item: satLabelTitleMath, attribute: .left, relatedBy: .equal, toItem: satBubble, attribute: .left, multiplier: 1.0, constant: satLabelWidth)
        let mathWidthConstraint = NSLayoutConstraint(item: satLabelTitleMath, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: satLabelWidth)
        let mathHeightConstraint = NSLayoutConstraint(item: satLabelTitleMath, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: satLabelHeight)
        satLabelTitleMath.addConstraints([mathWidthConstraint, mathHeightConstraint])
        satBubble.addConstraints([mathTopConstraint, mathLeftConstraint])
        
        satLabelTitleWriting = UILabel(frame: dummyRect)
        satLabelTitleWriting.textAlignment = .center
        satLabelTitleWriting.textColor = UIColor.white
        satLabelTitleWriting.font = headerFont
        satLabelTitleWriting.text = "Writing"
        satLabelTitleWriting.translatesAutoresizingMaskIntoConstraints = false
        
        satBubble.addSubview(satLabelTitleWriting)
        let writingTopConstraint = NSLayoutConstraint(item: satLabelTitleWriting, attribute: .top, relatedBy: .equal, toItem: satBubble, attribute: .top, multiplier: 1.0, constant: 7.0)
        let writingLeftConstraint = NSLayoutConstraint(item: satLabelTitleWriting, attribute: .left, relatedBy: .equal, toItem: satBubble, attribute: .left, multiplier: 1.0, constant: satLabelWidth * 2)
        let writingWidthConstraint = NSLayoutConstraint(item: satLabelTitleWriting, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: satLabelWidth)
        let writingHeightConstraint = NSLayoutConstraint(item: satLabelTitleWriting, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: satLabelHeight)
        satLabelTitleWriting.addConstraints([writingWidthConstraint, writingHeightConstraint])
        satBubble.addConstraints([writingTopConstraint, writingLeftConstraint])
        
        
        
        satLabelScoreReading = UILabel(frame: dummyRect)
        satLabelScoreReading.textAlignment = .center
        satLabelScoreReading.textColor = UIColor.white
        satLabelScoreReading.font = headerFont
        satLabelScoreReading.text = scoreReading
        satLabelScoreReading.translatesAutoresizingMaskIntoConstraints = false
        
        satBubble.addSubview(satLabelScoreReading)
        let readingScoreTopConstraint = NSLayoutConstraint(item: satLabelScoreReading, attribute: .top, relatedBy: .equal, toItem: satBubble, attribute: .top, multiplier: 1.0, constant: satLabelHeight)
        let readingScoreLeftConstraint = NSLayoutConstraint(item: satLabelScoreReading, attribute: .left, relatedBy: .equal, toItem: satBubble, attribute: .left, multiplier: 1.0, constant: 0.0)
        let readingScoreWidthConstraint = NSLayoutConstraint(item: satLabelScoreReading, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: satLabelWidth)
        let readingScoreHeightConstraint = NSLayoutConstraint(item: satLabelScoreReading, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: satLabelHeight)
        satLabelScoreReading.addConstraints([readingScoreWidthConstraint, readingScoreHeightConstraint])
        satBubble.addConstraints([readingScoreTopConstraint, readingScoreLeftConstraint])
        
        
        
        satLabelScoreMath = UILabel(frame: dummyRect)
        satLabelScoreMath.textAlignment = .center
        satLabelScoreMath.textColor = UIColor.white
        satLabelScoreMath.font = headerFont
        satLabelScoreMath.text = scoreMath
        satLabelScoreMath.translatesAutoresizingMaskIntoConstraints = false
        
        satBubble.addSubview(satLabelScoreMath)
        let mathScoreTopConstraint = NSLayoutConstraint(item: satLabelScoreMath, attribute: .top, relatedBy: .equal, toItem: satBubble, attribute: .top, multiplier: 1.0, constant: satLabelHeight)
        let mathScoreLeftConstraint = NSLayoutConstraint(item: satLabelScoreMath, attribute: .left, relatedBy: .equal, toItem: satBubble, attribute: .left, multiplier: 1.0, constant: satLabelWidth)
        let mathScoreWidthConstraint = NSLayoutConstraint(item: satLabelScoreMath, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: satLabelWidth)
        let mathScoreHeightConstraint = NSLayoutConstraint(item: satLabelScoreMath, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: satLabelHeight)
        satLabelScoreMath.addConstraints([mathScoreWidthConstraint, mathScoreHeightConstraint])
        satBubble.addConstraints([mathScoreTopConstraint, mathScoreLeftConstraint])
        
        
        
        
        satLabelScoreWriting = UILabel(frame: dummyRect)
        satLabelScoreWriting.textAlignment = .center
        satLabelScoreWriting.textColor = UIColor.white
        satLabelScoreWriting.font = headerFont
        satLabelScoreWriting.text = scoreWriting
        satLabelScoreWriting.translatesAutoresizingMaskIntoConstraints = false
        
        satBubble.addSubview(satLabelScoreWriting)
        let writingScoreTopConstraint = NSLayoutConstraint(item: satLabelScoreWriting, attribute: .top, relatedBy: .equal, toItem: satBubble, attribute: .top, multiplier: 1.0, constant: satLabelHeight)
        let writingScoreLeftConstraint = NSLayoutConstraint(item: satLabelScoreWriting, attribute: .left, relatedBy: .equal, toItem: satBubble, attribute: .left, multiplier: 1.0, constant: satLabelWidth * 2)
        let writingScoreWidthConstraint = NSLayoutConstraint(item: satLabelScoreWriting, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: satLabelWidth)
        let writingScoreHeightConstraint = NSLayoutConstraint(item: satLabelScoreWriting, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: satLabelHeight)
        satLabelScoreWriting.addConstraints([writingScoreWidthConstraint, writingScoreHeightConstraint])
        satBubble.addConstraints([writingScoreTopConstraint, writingScoreLeftConstraint])
        
        
        
        
        top += satBubbleHeight + 10.0
        
        //Now we get all our sexy sports stuff going.
        //(Ignore when there is no sports stuff)
        if sports1.count > 0 || sports2.count > 0 || sports3.count > 0 {
            sportsTitleLabel = UILabel(frame: dummyRect)
            sportsTitleLabel.font = headerFont
            sportsTitleLabel.textAlignment = .center
            sportsTitleLabel.text = "Available Sports"
            sportsTitleLabel.translatesAutoresizingMaskIntoConstraints = false
            scrollView.addSubview(sportsTitleLabel)
            let sportLabelTopConstraint = NSLayoutConstraint(item: sportsTitleLabel, attribute: .top, relatedBy: .equal, toItem: scrollView, attribute: .top, multiplier: 1.0, constant: top)
            let sportLabelCenterXConstraint = NSLayoutConstraint(item: sportsTitleLabel, attribute: .centerX, relatedBy: .equal, toItem: scrollView, attribute: .centerX, multiplier: 1.0, constant: 0.0)
            let sportLabelWidthConstraint = NSLayoutConstraint(item: sportsTitleLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 220.0)
            let sportLabelHeightConstraint = NSLayoutConstraint(item: sportsTitleLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 19.9)
            sportsTitleLabel.addConstraints([sportLabelWidthConstraint, sportLabelHeightConstraint])
            scrollView.addConstraints([sportLabelTopConstraint, sportLabelCenterXConstraint])
            
            
            top += 24.0
            
            
            
            
            if sports1.count > 0 {
                sportsLabel1 = UILabel(frame: dummyRect)
                sportsLabel1.font = descriptionFont
                sportsLabel1.textAlignment = .left
                sportsLabel1.numberOfLines = 0
                sportsLabel1.text = sports1
                sportsLabel1.translatesAutoresizingMaskIntoConstraints = false
                scrollView.addSubview(sportsLabel1)
                let sportsHeight: CGFloat = CGFloat(UILabel.getHeight(ofString: sports1, font: descriptionFont, width: Float(descriptionWidth), maxLines: 0)) + 3.0
                let labelTopConstraint = NSLayoutConstraint(item: sportsLabel1, attribute: .top, relatedBy: .equal, toItem: scrollView, attribute: .top, multiplier: 1.0, constant: top)
                let labelCenterXConstraint = NSLayoutConstraint(item: sportsLabel1, attribute: .centerX, relatedBy: .equal, toItem: scrollView, attribute: .centerX, multiplier: 1.0, constant: 0.0)
                let labelWidthConstraint = NSLayoutConstraint(item: sportsLabel1, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: descriptionWidth)
                let labelHeightConstraint = NSLayoutConstraint(item: sportsLabel1, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: sportsHeight)
                sportsLabel1.addConstraints([labelWidthConstraint, labelHeightConstraint])
                scrollView.addConstraints([labelTopConstraint, labelCenterXConstraint])
                top += (sportsHeight + 4.0)
            }
            
            
            if sports2.count > 0 {
                sportsLabel2 = UILabel(frame: dummyRect)
                sportsLabel2.font = descriptionFont
                sportsLabel2.textAlignment = .left
                sportsLabel2.numberOfLines = 0
                sportsLabel2.text = sports2
                sportsLabel2.translatesAutoresizingMaskIntoConstraints = false
                scrollView.addSubview(sportsLabel2)
                let sportsHeight: CGFloat = CGFloat(UILabel.getHeight(ofString: sports2, font: descriptionFont, width: Float(descriptionWidth), maxLines: 0)) + 3.0
                let labelTopConstraint = NSLayoutConstraint(item: sportsLabel2, attribute: .top, relatedBy: .equal, toItem: scrollView, attribute: .top, multiplier: 1.0, constant: top)
                let labelCenterXConstraint = NSLayoutConstraint(item: sportsLabel2, attribute: .centerX, relatedBy: .equal, toItem: scrollView, attribute: .centerX, multiplier: 1.0, constant: 0.0)
                let labelWidthConstraint = NSLayoutConstraint(item: sportsLabel2, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: descriptionWidth)
                let labelHeightConstraint = NSLayoutConstraint(item: sportsLabel2, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: sportsHeight)
                sportsLabel2.addConstraints([labelWidthConstraint, labelHeightConstraint])
                scrollView.addConstraints([labelTopConstraint, labelCenterXConstraint])
                top += (sportsHeight + 4.0)
            }
            
            
            if sports3.count > 0 {
                sportsLabel3 = UILabel(frame: dummyRect)
                sportsLabel3.font = descriptionFont
                sportsLabel3.textAlignment = .left
                sportsLabel3.numberOfLines = 0
                sportsLabel3.text = sports3
                sportsLabel3.translatesAutoresizingMaskIntoConstraints = false
                scrollView.addSubview(sportsLabel3)
                let sportsHeight: CGFloat = CGFloat(UILabel.getHeight(ofString: sports3, font: descriptionFont, width: Float(descriptionWidth), maxLines: 0)) + 3.0
                let labelTopConstraint = NSLayoutConstraint(item: sportsLabel3, attribute: .top, relatedBy: .equal, toItem: scrollView, attribute: .top, multiplier: 1.0, constant: top)
                let labelCenterXConstraint = NSLayoutConstraint(item: sportsLabel3, attribute: .centerX, relatedBy: .equal, toItem: scrollView, attribute: .centerX, multiplier: 1.0, constant: 0.0)
                let labelWidthConstraint = NSLayoutConstraint(item: sportsLabel3, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: descriptionWidth)
                let labelHeightConstraint = NSLayoutConstraint(item: sportsLabel3, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: sportsHeight)
                sportsLabel3.addConstraints([labelWidthConstraint, labelHeightConstraint])
                scrollView.addConstraints([labelTopConstraint, labelCenterXConstraint])
                top += (sportsHeight + 4.0)
            }
            
        }
        
        scrollView.contentSize = CGSize(width: view.bounds.size.width, height: top + 300.0)
        
    }
    
}

