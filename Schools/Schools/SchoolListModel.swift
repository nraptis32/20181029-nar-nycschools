//
//  SchoolListModel.swift
//  Schools
//
//  Created by Nicholas Raptis on 10/30/18.
//  Copyright © 2018 Chase. All rights reserved.
//

import UIKit
import CoreData
import SQLite3

protocol SchoolListDelegate: class {
    func schoolList(didUpdate schoolList: SchoolListModel)
}

class SchoolListModel: NSObject {
    
    weak var delegate: SchoolListDelegate?
    
    // Make sure nobody gets naughty with multi-threading.
    // Only one person can talk to schoolData at once, please.
    let semaphor = DispatchSemaphore(value: 1)
    
    // Managed objects which are managed by the management manager.
    var schoolData: [NSManagedObject] = []
    
    // Quick banger to grab the count, easy enough.
    var count: Int {
        return schoolData.count
    }
    
    // We can't access this on background thread, so
    // we keep as a local variable.
    private var appDelegate: AppDelegate!
    
    /// First try to load from CoreData (lightning quick)
    /// if it doesn't exist, populate our CoreData store.
    func load() {
        appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let loadingQueue = DispatchQueue(label: "fetch_queue", qos: .userInitiated)
        loadingQueue.async {
            self.semaphor.wait()
            self.attemptLoadCoreData()
            if self.schoolData.count <= 0 {
                self.attemptLoadSQLLite()
            }
            self.semaphor.signal()
            self.delegate?.schoolList(didUpdate: self)
        }
    }
    
    
    /// The user is returning and we have already got our
    /// school info into CoreData?
    private func attemptLoadCoreData() {
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "School")
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            schoolData = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    /// This is required to work for first launch of the app.
    /// We load the SQLLite database that is bundles with the app...
    private func attemptLoadSQLLite() {
        
        let filePath = CSVStripper.bundleDir + "school_data.sqlite"
        var schoolList = [SchoolModel]()
        
        var db: OpaquePointer!
        if sqlite3_open(filePath, &db) == SQLITE_OK {
            var selectStatement: OpaquePointer!
            if sqlite3_prepare_v2(db, "select * from school;", -1, &selectStatement, nil) == SQLITE_OK {
                while sqlite3_step(selectStatement) == SQLITE_ROW {
                    
                    let school = SchoolModel()
                    
                    school.uuid = String(cString: sqlite3_column_text(selectStatement, 0))
                    school.name = String(cString: sqlite3_column_text(selectStatement, 1))
                    school.description = String(cString: sqlite3_column_text(selectStatement, 2))
                    school.sports1 = String(cString: sqlite3_column_text(selectStatement, 3))
                    school.sports2 = String(cString: sqlite3_column_text(selectStatement, 4))
                    school.sports3 = String(cString: sqlite3_column_text(selectStatement, 5))
                    school.zone = String(cString: sqlite3_column_text(selectStatement, 6))
                    school.city = String(cString: sqlite3_column_text(selectStatement, 7))
                    
                    school.satTakerCount = Int(sqlite3_column_int(selectStatement, 8))
                    school.satReadingAvg = Int(sqlite3_column_int(selectStatement, 9))
                    school.satMathAvg = Int(sqlite3_column_int(selectStatement, 10))
                    school.satWritingAvg = Int(sqlite3_column_int(selectStatement, 11))
                    
                    schoolList.append(school)
                }
            }
            sqlite3_finalize(selectStatement)
            sqlite3_close(db)
        }
        
        dumpToCoreData(&schoolList)
        
    }
    
    
    /// Take a list of SchoolModel objects and fire them all
    /// inside of core data. We make separate entities for
    /// city and region/zone, to prevent duplicate storage.
    ///
    /// - Parameter schoolList: The list of SchoolModel objects (populated)
    func dumpToCoreData(_ schoolList: inout [SchoolModel]) {
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        var cityMap:[String:NSManagedObject] = [:]
        let cityEntity = NSEntityDescription.entity(forEntityName: "City",
                                                    in: managedContext)!
        for school in schoolList {
            if school.city.count > 0 {
                
                if cityMap[school.city] != nil {
                    continue
                }
                
                let city = NSManagedObject(entity: cityEntity, insertInto: managedContext)
                city.setValue(school.city, forKey: "name")
                cityMap[school.city] = city
            }
        }
        
        var regionMap:[String:NSManagedObject] = [:]
        let regionEntity = NSEntityDescription.entity(forEntityName: "Region",
                                                      in: managedContext)!
        for school in schoolList {
            if school.zone.count > 0 {
                if regionMap[school.zone] != nil {
                    continue
                }
                
                let region = NSManagedObject(entity: regionEntity, insertInto: managedContext)
                region.setValue(school.zone, forKey: "name")
                regionMap[school.zone] = region
            }
        }
        
        let schoolEntity = NSEntityDescription.entity(forEntityName: "School",
                                                      in: managedContext)!
        for school in schoolList {
            if school.zone.count > 0 {
                let existsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "School")
                existsRequest.returnsObjectsAsFaults = false
                existsRequest.predicate = NSPredicate(format: "uuid = %@", school.uuid)
                do {
                    let existing = try managedContext.fetch(existsRequest)
                    if existing.count > 0 {
                        print("Already Have UUID \(school.uuid)")
                        continue
                    }
                } catch {
                    
                }
                
                let schoolObject = NSManagedObject(entity: schoolEntity, insertInto: managedContext)
                
                schoolObject.setValue(school.uuid, forKey: "uuid")
                schoolObject.setValue(school.name, forKey: "name")
                schoolObject.setValue(school.description, forKey: "long_description")
                
                //Crucial to get the sports.
                schoolObject.setValue(school.sports1, forKey: "sports1")
                schoolObject.setValue(school.sports2, forKey: "sports2")
                schoolObject.setValue(school.sports3, forKey: "sports3")
                
                //Get the test scores, required for small assignment.
                schoolObject.setValue(school.satReadingAvg, forKey: "sat_reading")
                schoolObject.setValue(school.satMathAvg, forKey: "sat_math")
                schoolObject.setValue(school.satWritingAvg, forKey: "sat_writing")
                
                //Extra credit: Let's derive some search terms based on NAME.
                let keywords = String.keywords(fromString: school.name)
                var index: Int = 1
                for keyword in keywords {
                    if keyword.count > 0 {
                        schoolObject.setValue(keyword, forKey: "search_word_\(index)")
                        index += 1
                        if index > 12 {
                            break
                        }
                    }
                }
                
                if let city = cityMap[school.city] {
                    schoolObject.setValue(city, forKey: "city")
                }
                if let region = regionMap[school.zone] {
                    schoolObject.setValue(region, forKey: "region")
                }
                schoolData.append(schoolObject)
            }
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    
    /// Narrow down school list with search text.
    ///
    /// - Parameter searchString: Whatever the user
    ///   types as text, right out of the search box.
    func filterResults(_ searchString: String) {
        
        self.semaphor.wait()
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "School")
        fetchRequest.returnsObjectsAsFaults = false
        
        let keywords = String.keywords(fromString: searchString)
        
        if keywords.count > 0 {
            var predicateList = [NSPredicate]()
            for keyword in keywords {
                let predicate = NSPredicate(format: "(search_word_1 BEGINSWITH[cd] %@) OR (search_word_2 BEGINSWITH[cd] %@) OR (search_word_3 BEGINSWITH[cd] %@) OR (search_word_4 BEGINSWITH[cd] %@) OR (search_word_5 BEGINSWITH[cd] %@) OR (search_word_6 BEGINSWITH[cd] %@) OR (search_word_7 BEGINSWITH[cd] %@) OR (search_word_8 BEGINSWITH[cd] %@) OR (search_word_9 BEGINSWITH[cd] %@) OR (search_word_10 BEGINSWITH[cd] %@) OR (search_word_11 BEGINSWITH[cd] %@) OR (search_word_12 BEGINSWITH[cd] %@)", keyword, keyword, keyword, keyword, keyword, keyword, keyword, keyword, keyword, keyword, keyword, keyword)
                predicateList.append(predicate)
            }
            
            let compond = NSCompoundPredicate(andPredicateWithSubpredicates: predicateList)
            
            fetchRequest.predicate = compond
        }
        
        do {
            schoolData = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch compound. \(error), \(error.userInfo)")
        }
        
        self.semaphor.signal()
        
        self.delegate?.schoolList(didUpdate: self)
    }
}
