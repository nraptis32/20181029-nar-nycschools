//
//  SchoolListViewController.swift
//  Schools
//
//  Created by Nicholas Raptis on 10/30/18.
//  Copyright © 2018 Chase. All rights reserved.
//

import UIKit
import CoreData

class SchoolListViewController: UIViewController {
    
    
    
    let schoolList = SchoolListModel()
    var deviceWidth: CGFloat = 320.0
    
    weak var selectedManagedObject: NSManagedObject?
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            //Using a lower cast for scroll view.
            let scrollView = tableView as UIScrollView
            scrollView.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        deviceWidth = min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height)
        
        self.title = "New York Schools"
        
        schoolList.delegate = self
        schoolList.load()
    }
    
    
    /// Refresh the table view, inside of semaphor barrier
    /// to prevent multiple threads from accessing managed schools.
    func reloadFiltered() {
        schoolList.semaphor.wait()
        self.tableView.reloadData()
        schoolList.semaphor.signal()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "school_detail" {
            if let detail = segue.destination as? SchoolDetailViewController, let managed = selectedManagedObject {
                detail.loadViewIfNeeded()
                detail.setUpWith(managed: managed)
            }
        }
    }
}

extension SchoolListViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolList.count
    }
    
    /// Go fetch a school cell from the storyboard.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "school_cell", for: indexPath) as! SchoolTableCell
        cell.setUpWith(managed: schoolList.schoolData[indexPath.row])
        return cell
    }
    
    /// We clicked one of our high schools, hot hot hot
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row >= 0 && indexPath.row < schoolList.count {
            searchBar.resignFirstResponder()
            selectedManagedObject = schoolList.schoolData[indexPath.row]
            self.performSegue(withIdentifier: "school_detail", sender: nil)
        }
    }
    
    /// Our rows will be different heights depending on
    /// how big they in high school named get much longer.
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row >= 0 && indexPath.row < schoolList.count {
            let managed = schoolList.schoolData[indexPath.row]
            let name = managed.value(forKeyPath: "name") as! String
            let areaWidth = Float(deviceWidth - (74 + 8 + 14))
            let fontHeight = UILabel.getHeight(ofString: name, font: SchoolTableCell.cellTitleFont, width: areaWidth, maxLines: 0)
            return CGFloat(fontHeight + 18.0 + 6.0 + 2.0 + 6.0)
        }
        return 75.0
    }
}

extension SchoolListViewController: SchoolListDelegate {
    
    
    /// Our delegate method that lets us know the
    /// managed schools are changing, need to redisplay!
    ///
    /// - Parameter schoolList: The school list model.
    func schoolList(didUpdate schoolList: SchoolListModel) {
        DispatchQueue.main.async {
            self.reloadFiltered()
        }
    }
}

extension SchoolListViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //We wanted to dismiss the keyboard when we scroll.
        //Sadly, scroll events are fired as the user types.
    }
}

extension SchoolListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        schoolList.filterResults(searchText)
    }
}

