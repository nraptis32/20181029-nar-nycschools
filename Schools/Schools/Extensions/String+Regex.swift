//
//  String+Regex.swift
//  Schools
//
//  Created by Nicholas Raptis on 10/30/18.
//  Copyright © 2018 Chase. All rights reserved.
//

import Foundation

extension String {
    
    
    /// Splits the string using a regex pattern.
    ///
    /// - Parameter pattern: The regex in string format.
    /// - Returns: Array of strings, split by the regex.
    func split(regex pattern: String) -> [String] {
        guard let regex = try? NSRegularExpression(pattern: pattern, options: [])
            else { return [] }
        let result = regex.stringByReplacingMatches(in: self, options: [], range: NSRange(location: 0, length: (self as NSString).length), withTemplate: "\0")
        return result.components(separatedBy: "\0")
    }
}
