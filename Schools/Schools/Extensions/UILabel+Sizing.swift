//
//  UILabel+Sizing.swift
//  Schools
//
//  Created by Nicholas Raptis on 10/31/18.
//  Copyright © 2018 Chase. All rights reserved.
//

import UIKit

extension UILabel {
    
    
    /// Get the height that a string would require the label to be
    /// to fit properly using the width + font of THIS label...
    ///
    /// - Parameter string: The string which we are trying to fit.
    /// - Returns: Height for THIS label which properly fits the input string.
    func getHeight(ofString string: String) -> Float {
        return UILabel.getHeight(ofString: string, font: font, width: Float(bounds.width), maxLines: numberOfLines)
    }
    
    
    /// Get the height that a string would require the label to be
    /// to fit properly the specified width, font, and lineCount
    ///
    /// - Parameters:
    ///   - string: The string which we are trying to fit.
    ///   - font: The font we are using.
    ///   - width: Width of expected label.
    ///   - maxLines: Line count of expected label (use 0)
    /// - Returns: Height for theoretical label to fit the input string.
    class func getHeight(ofString string: String, font: UIFont, width: Float, maxLines: Int) -> Float {
        let label =  UILabel(frame: CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(width), height: CGFloat(9999999.0)))
        label.numberOfLines = maxLines
        label.font = font
        label.text = string
        label.sizeToFit()
        return Float(label.frame.height)
    }
    
    func getWidth(ofString string: String) -> Float {
        return UILabel.getWidth(ofString: string, font: font, width: 9999999.0, maxLines: numberOfLines)
    }
    
    class func getWidth(ofString string: String, font: UIFont, width: Float, maxLines: Int) -> Float {
        let label =  UILabel(frame: CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(width), height: CGFloat(9999999.0)))
        label.numberOfLines = maxLines
        label.font = font
        label.text = string
        label.sizeToFit()
        return Float(label.frame.width)
    }
    
}
