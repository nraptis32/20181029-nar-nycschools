//
//  String+Utils.swift
//  Schools
//
//  Created by Nicholas Raptis on 10/31/18.
//  Copyright © 2018 Chase. All rights reserved.
//

import Foundation

extension String {
    
    
    /// Splits a string into an array of keywords using common delimeters.
    ///
    /// - Parameter string: String (for example search bar text)
    /// - Returns: Split apart and cleaned up array of individual search terms.
    static func keywords(fromString string: String?) -> [String] {
        var result = [String]()
        guard let originalText = string else { return result } //Only dirtbags pass null strings.
        let text = originalText.lowercased() //If the user wants to use HaXoR tYpe than k00l.
        let splitterString = ",:;.-_/\\*^|&" //Some extra splitters (mainly commas)
        var splitterSet = CharacterSet(charactersIn: splitterString)
        splitterSet = CharacterSet.whitespacesAndNewlines.union(splitterSet)
        let textChunks = text.components(separatedBy: splitterSet)
        for chunk in textChunks {
            let trimmedChunk = chunk.trimmingCharacters(in: .whitespacesAndNewlines)
            if trimmedChunk.count > 0 { result.append(trimmedChunk) } //Ignore empty words
        }
        return result
    }
    
}
