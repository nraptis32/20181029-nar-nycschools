//
//  InfoBubbleView.swift
//  Schools
//
//  Created by Nicholas Raptis on 10/31/18.
//  Copyright © 2018 Chase. All rights reserved.
//

import UIKit

class InfoBubbleView: UIView {
    
    var cornerUL = true
    var cornerUR = true
    var cornerDR = true
    var cornerDL = true
    var fill:Bool = true
    var fillColor = UIColor(red: 0.239, green: 0.718, blue: 0.12157, alpha: 1.0)
    var stroke:Bool = true
    var strokeColor = UIColor(red: 0.239 * 0.8, green: 0.718 * 0.8, blue: 0.12157 * 0.8, alpha: 1.0)
    var strokeWidth:CGFloat = 1.0
    var cornerRadius:CGFloat = 18.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        isOpaque = false
        clearsContextBeforeDrawing = true
        backgroundColor = UIColor.white
    }

    class func getCornerType(ul:Bool, ur:Bool, dr:Bool, dl:Bool) -> UIRectCorner {
        var result:UIRectCorner = UIRectCorner(rawValue: 0)
        if ul == true {result = result.union(UIRectCorner.topLeft)}
        if ur == true {result = result.union(UIRectCorner.topRight)}
        if dr == true {result = result.union(UIRectCorner.bottomRight)}
        if dl == true {result = result.union(UIRectCorner.bottomLeft)}
        return result;
    }
    
    override func draw(_ rect: CGRect) {
        
        super.draw(rect)

        
        let context: CGContext = UIGraphicsGetCurrentContext()!
        context.saveGState()
        
        context.setFillColor(UIColor.white.cgColor)
        context.fill(bounds)
        
        var rect = bounds
        
        if true {
            let clipPath = UIBezierPath(roundedRect: rect, byRoundingCorners: InfoBubbleView.getCornerType(ul: cornerUL, ur: cornerUR, dr: cornerDR, dl: cornerDL), cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
            context.saveGState()
            context.beginPath()
            context.addPath(clipPath)
            context.setFillColor(strokeColor.cgColor)
            context.closePath()
            context.fillPath()
            context.restoreGState()
            
            let inset = strokeWidth / 2.0
            rect = rect.insetBy(dx: inset, dy: inset)
        }
        
        if true {
            let clipPath = UIBezierPath(roundedRect: rect,
                                        byRoundingCorners: InfoBubbleView.getCornerType(ul: cornerUL, ur: cornerUR, dr: cornerDR, dl: cornerDL),
                                        cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
            context.saveGState()
            context.beginPath()
            context.addPath(clipPath)
            context.setFillColor(fillColor.cgColor)
            context.closePath()
            context.fillPath()
            context.restoreGState()
        }
        
        context.restoreGState()
    }
    
}
