//
//  SchoolTableCellAccent.swift
//  Schools
//
//  Created by Nicholas Raptis on 10/31/18.
//  Copyright © 2018 Chase. All rights reserved.
//

import UIKit

class SchoolTableCellAccent: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        setSymbolChevronRight(arr: &chevronPoints)
    }
    
    var chevronPoints = [CGPoint]()

    func setSymbolChevronRight(arr: inout [CGPoint]) {
        arr.removeAll()
        arr.append(CGPoint(x: 0.41, y: 0.14))
        arr.append(CGPoint(x: 0.44, y: 0.14))
        arr.append(CGPoint(x: 0.79, y: 0.49))
        arr.append(CGPoint(x: 0.79, y: 0.51))
        arr.append(CGPoint(x: 0.44, y: 0.86))
        arr.append(CGPoint(x: 0.41, y: 0.86))
        arr.append(CGPoint(x: 0.32, y: 0.77))
        arr.append(CGPoint(x: 0.32, y: 0.74))
        arr.append(CGPoint(x: 0.56, y: 0.50))
        arr.append(CGPoint(x: 0.32, y: 0.26))
        arr.append(CGPoint(x: 0.32, y: 0.23))
    }
    
    func path(fromPoints points: inout [CGPoint], inRect rect: CGRect) -> UIBezierPath {
        let path = UIBezierPath()
        for i in 0..<points.count {
            var point = CGPoint(x: points[i].x, y: points[i].y)
            point.x = rect.origin.x + rect.size.width * (point.x)
            point.y = rect.origin.y + rect.size.height * (point.y)
            if i == 0 {
                path.move(to: point)
            } else {
                path.addLine(to: point)
            }
        }
        return path
    }
    
    override func draw(_ rect: CGRect) {
        
        super.draw(rect)
        
        let context: CGContext = UIGraphicsGetCurrentContext()!
        context.saveGState()
        
        UIColor(red: CGFloat(17.0 / 255.0), green: CGFloat(122.0 / 255.0), blue: CGFloat(202.0 / 255.0), alpha: 1.0).setFill()
        
        let radius = bounds.midX * 0.54
        let circleCenter = CGPoint(x: bounds.midX, y: bounds.midY)
        let circlePath = UIBezierPath(arcCenter: circleCenter, radius: radius, startAngle: 0.0, endAngle: CGFloat.pi * 2.0, clockwise: true)
        circlePath.fill()
        
        let symbolSize = bounds.size.width * 0.375
        let symbolFrame = CGRect(x: circleCenter.x - symbolSize / 2.0, y: circleCenter.y - symbolSize / 2.0, width: symbolSize, height: symbolSize)
        let path = self.path(fromPoints: &chevronPoints, inRect: symbolFrame)
        context.beginPath()
        context.addPath(path.cgPath)
        context.closePath()
        context.setFillColor(UIColor.white.cgColor)
        context.fillPath()
        
        context.restoreGState()
        
    }
    
}
