//
//  CSVRipper.swift
//  Schools
//
//  Created by Nicholas Raptis on 10/29/18.
//  Copyright © 2018 Chase. All rights reserved.
//

import UIKit
import SQLite3

//This is used when we get a new data dump
//from the provider. We convert the heavy CSV
//into a lighter and nimbler SQLLite db.
class CSVStripper {
    
    static var schoolList = [SchoolModel]()
    
    //Hash table of our schools based on UUID,
    //using for quick fetch assigning SAT scores.
    static var schoolLUT = [String:SchoolModel]()
    
    static var bundleDir: String {
        var result = Bundle.main.resourcePath ?? "MAIN_BUNDLE_MISSING"
        result = result + "/"
        return result
    }
    
    static var docsDir: String {
        var result:String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        result = result + "/"
        return result
    }
    
    //Regex that matches CSV with quoted portions...
    static let quotePattern = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"
    
    //Load the school directory CSV file and convert
    //into our model objects.
    static func parseDirectoryInfo() {
        
        let filePath = bundleDir + "2017_DOE_High_School_Directory.csv"
        guard let data = try? Data(contentsOf: URL(fileURLWithPath: filePath)) else {
            print("Can't find High_School_Directory.csv")
            return
        }
        
        guard let fileData = String(data: data, encoding: .utf8) else {
            print("Can't convert High_School_Directory.csv")
            return
        }

        var lines = fileData.split(separator: "\n")
        lines.removeFirst()
        
        for line in lines {
            let tidbits = String(line).split(regex: quotePattern)
            if tidbits.count >= 462 && tidbits[0].count > 3 {
                let school = SchoolModel()
                
                school.uuid = tidbits[0].trimmingCharacters(in: .whitespacesAndNewlines)
                school.name = tidbits[1].trimmingCharacters(in: .whitespacesAndNewlines)
                school.description = tidbits[3].trimmingCharacters(in: .whitespacesAndNewlines)
                school.sports1 = tidbits[33].trimmingCharacters(in: .whitespacesAndNewlines)
                school.sports2 = tidbits[34].trimmingCharacters(in: .whitespacesAndNewlines)
                school.sports3 = tidbits[35].trimmingCharacters(in: .whitespacesAndNewlines)
                school.zone = tidbits[460].trimmingCharacters(in: .whitespacesAndNewlines)
                school.city = tidbits[461].trimmingCharacters(in: .whitespacesAndNewlines)
                
                schoolList.append(school)
                schoolLUT[school.uuid] = school
            }
        }
        parseSATResults()
    }
    
    //Load the SAT file and marry the data to
    //our previously loaded school directory.
    static func parseSATResults() {
        
        let filePath = bundleDir + "2012_SAT_Results.csv"
        guard let data = try? Data(contentsOf: URL(fileURLWithPath: filePath)) else {
            print("Can't find 2012_SAT_Results.csv")
            return
        }
        
        guard let fileData = String(data: data, encoding: .utf8) else {
            print("Can't convert 2012_SAT_Results.csv")
            return
        }
        
        var lines = fileData.split(separator: "\n")
        lines.removeFirst()
        
        for line in lines {
            let tidbits = String(line).split(regex: quotePattern)
            
            if tidbits.count >= 6 && tidbits[0].count > 3 {
                let uuid = tidbits[0]
                
                if let school = schoolLUT[uuid] {
                    school.didMatch += 1
                    school.satTakerCount = Int(tidbits[2].trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
                    school.satReadingAvg = Int(tidbits[3].trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
                    school.satMathAvg = Int(tidbits[4].trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
                    school.satWritingAvg = Int(tidbits[5].trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
                }
            }
        }
        exportSQLite()
    }
    
    //We export the combined db to our documents
    //directory as a SQLLite db. Can grab file from HD.
    static func exportSQLite() {
        
        let filePath = docsDir + "school_data.sqlite"
        
        print("Sqlite Path: ")
        print(filePath)
        
        var db: OpaquePointer!
        if sqlite3_open(filePath, &db) == SQLITE_OK {
            
            //All them rows.
            let createSchoolTableString = "create table school(id char(64) primary key not null, name char(512), description char(4096), sports1 char(2048), sports2 char(2048), sports3 char(2048), zone char(256), city char(256), satCount int, satReadingAvg int, satMathAvg int, satWritingAvg int)"
            var createSchoolTableStatement: OpaquePointer? = nil
            
            
            if sqlite3_prepare_v2(db, createSchoolTableString, -1, &createSchoolTableStatement, nil) == SQLITE_OK {
                // 3
                if sqlite3_step(createSchoolTableStatement) == SQLITE_DONE {
                    print("Contact table created.")
                } else {
                    print("Contact table could not be created.")
                }
            } else {
                print("CREATE TABLE statement could not be prepared.")
            }
            // 4
            sqlite3_finalize(createSchoolTableStatement)
            

            let insertSchoolStatementString = "insert into school(id, name, description, sports1, sports2, sports3, zone, city, satCount, satReadingAvg, satMathAvg, satWritingAvg) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);"
            
            for school in schoolList {
                var insertSchoolStatement: OpaquePointer? = nil
                if sqlite3_prepare_v2(db, insertSchoolStatementString, -1, &insertSchoolStatement, nil) == SQLITE_OK {
                    
                    sqlite3_bind_text(insertSchoolStatement, 1, (school.uuid as NSString).utf8String, -1, nil)
                    
                    sqlite3_bind_text(insertSchoolStatement, 2, (school.name as NSString).utf8String, -1, nil)
                    sqlite3_bind_text(insertSchoolStatement, 3, (school.description as NSString).utf8String, -1, nil)
                    
                    sqlite3_bind_text(insertSchoolStatement, 4, (school.sports1 as NSString).utf8String, -1, nil)
                    sqlite3_bind_text(insertSchoolStatement, 5, (school.sports2 as NSString).utf8String, -1, nil)
                    sqlite3_bind_text(insertSchoolStatement, 6, (school.sports3 as NSString).utf8String, -1, nil)
                    
                    sqlite3_bind_text(insertSchoolStatement, 7, (school.zone as NSString).utf8String, -1, nil)
                    sqlite3_bind_text(insertSchoolStatement, 8, (school.city as NSString).utf8String, -1, nil)
                    
                    sqlite3_bind_int(insertSchoolStatement, 9, Int32(school.satTakerCount))
                    sqlite3_bind_int(insertSchoolStatement, 10, Int32(school.satReadingAvg))
                    sqlite3_bind_int(insertSchoolStatement, 11, Int32(school.satMathAvg))
                    sqlite3_bind_int(insertSchoolStatement, 12, Int32(school.satMathAvg))
                    
                    if sqlite3_step(insertSchoolStatement) == SQLITE_DONE {
                        
                    } else {
                        print("Failed to insert school.")
                    }
                    
                    sqlite3_finalize(insertSchoolStatement)
                }
            }
            sqlite3_close(db)
        }
    }
    
}
