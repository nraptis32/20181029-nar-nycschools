//
//  SchoolTableCell.swift
//  Schools
//
//  Created by Nicholas Raptis on 10/30/18.
//  Copyright © 2018 Chase. All rights reserved.
//

import UIKit
import CoreData

class SchoolTableCell: UITableViewCell {
    
    static let cellTitleFont = UIFont(name: "HelveticaNeue", size: 16.0)!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var regionLabel: UILabel!
    
    @IBOutlet weak var nameLabelHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cityLabelWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cellAccent: SchoolTableCellAccent!
    
    /// We're alive, out of the story board / NIB.
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.font = SchoolTableCell.cellTitleFont
    }
    
    /// Re-assign this cell to a new managed school object.
    ///
    /// - Parameter managed: The school data model, which is being managed.
    func setUpWith(managed: NSManagedObject) {
        
        var name = managed.value(forKeyPath: "name") as! String
        name = name.trimmingCharacters(in: CharacterSet(charactersIn: "\""))
        
        nameLabel.text = name
        
        if let city = managed.value(forKey: "city") as? NSManagedObject {
            let cityName = city.value(forKeyPath: "name") as! String
            cityLabelWidthConstraint.constant = CGFloat(cityLabel.getWidth(ofString: cityName) + 4.0)
            cityLabel.text = cityName
        }
        
        if let region = managed.value(forKey: "region") as? NSManagedObject {
            let regionName = region.value(forKeyPath: "name") as! String
            regionLabel.text = regionName
        }
        
        let areaWidth = Float(bounds.size.width - (74 + 8 + 14))
        let fontHeight = UILabel.getHeight(ofString: name, font: SchoolTableCell.cellTitleFont, width: areaWidth, maxLines: 0)
        nameLabelHeightConstraint.constant = CGFloat(fontHeight)
    }
    
    
    /// We're about to re-use this cell, wipe the labels
    /// using built in cocoa hook.
    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = ""
        cityLabel.text = ""
        regionLabel.text = ""
    }
    
}


